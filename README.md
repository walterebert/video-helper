# Video Helper

A JavaScript library to assist video playback in browsers

## Examples

### Detect video support
```
var vh = new VideoHelper,
    video = false;

if (vh.video) {
    if (vh.dash) {
      video = "video.mpd";
    } else if (vh.hls) {
      video = "video.m3u8";
    } else if (vh.h264) {
      video = "video.mp4";
    } else if (vh.vp9Webm) {
      video = "video-vp9.webm";
    } else if (vh.webm) {
      video = "video.webm";
    }
}
```

### Determine video size based on screen size & pixel ratio
```
<video id="player" src="480x270.mp4" controls>
  <a href="video.mp4">Download Video</a>
</video>

<script>
var vh = new VideoHelper(),
    max = vh.maxScreen();

if (max >= 1920) {
  document.getElementById("player").setAttribute("src", "1920x1080.mp4");
} else if (max >= 1280) {
  document.getElementById("player").setAttribute("src", "1280x720.mp4");
}
</script>
```

### Detect autoplay support
This is not 100% reliable. Autoplay might be supported, even if the test result is negative.

```
<div id="autoplayer"></div>
<script>
var vh = new VideoHelper();

vh.testAutoplay(function(result) {
    if (result) {
        document.getElementById('autoplayer').innerHTML = '<video autoplay loop muted>' +
            '<source src="video.m3u8" type="application/x-mpeg-url">' +
            '<source src="video.mp4" type="video/mp4">' +
            '<source src="video.webm" type="video/webm">' +
            '<\/video>';
    } else {
        document.getElementById('autoplayer').innerHTML = '<img src="screenshot.jpg" alt="">';
    }
});
</script>
```

### Determine viewport/iframe size
The maxViewport function returns the largest dimension.

```
var vh = new VideoHelper(),
    max = vh.maxViewport();
```

### Use custom buttons
```
<video id="player" src="480x270.mp4" controls>
  <a href="480x270.mp4">Download Video</a>
</video>
<div id="buttons"></div>

<script>
var vh = new VideoHelper();

// Remove native controls
vh.hideControls(document.getElementById("player"));

// Add buttons
// see for example https://msdn.microsoft.com/en-us/library/hh924823%28v=vs.85%29.aspx
</script>
```

### Support for old browsers
```
<div id="player">
    <a href="video.mp4">Download Video</a>
</div>

<script>
// Set video size (default: width = 640, height = 360)
var vh = new VideoHelper({width: 480, height: 270}),
    video = false;

// Use Windows Media Player for IE8
if (vh.mp4Wmp) {
    video = "video.mp4";
} else if (vh.wmvWmp) {
    video = "video.wmv";
}
if (video) {
    vh.wmPlayer({id: "player", file: video});
}

// Use QuickTime Player
if (vh.mp4Quicktime) {
    video = "video.mp4";
} else if (vh.quicktime) {
    video = "video.mov";
}
if (video) {
    vh.quicktimePlayer({id: "player", file: video});
}
</script>
```

## License
[MIT](http://opensource.org/licenses/MIT)

## Author
[Walter Ebert](http://walterebert.com)
