/*!
Video Helper https://gitlab.com/walterebert/video-helper
(c) 2015-2021 Walter Ebert https://wee.plus
Licensed MIT https://opensource.org/licenses/MIT
*/

function VideoHelper(options) {
    // Set default options
    this.options = {
        width: 640,
        height: 360
    };

    // Custom options
    if (typeof options === "object") {
        if (options.width) {
            this.options.width = options.width;
        }
        if (options.height) {
            this.options.height = options.height;
        }
    }

    // Force an element to adjust to the size of an iframe
    this.fit = function (el) {
        if (typeof el === "object" && document.documentElement.clientWidth) {
            el.setAttribute("width", document.documentElement.clientWidth);
            el.setAttribute("height", document.documentElement.clientHeight);
        }
    };

    // Hide video control elements
    this.hideControls = function (el) {
        if (typeof el === "object") {
            el.removeAttribute("controls");
        }
    };

    // Find maximum screen dimension
    this.maxScreen = function () {
        var pixelRatio = 1,
            maxSize = this.options.width;

        if (window.screen.width) {
            maxSize = window.screen.width;
            if (window.screen.height > maxSize) {
                maxSize = window.screen.height;
            }
            if (window.hasOwnProperty("devicePixelRatio")) {
                pixelRatio = window.devicePixelRatio;
            }
            if (pixelRatio > 1) {
                maxSize = maxSize * pixelRatio;
            }
        }

        return maxSize;
    };

    // Find maximum viewport dimension
    this.maxViewport = function () {
        var maxSize = this.options.width;

        if (document.documentElement.clientWidth) {
            maxSize = document.documentElement.clientWidth;
            if (document.documentElement.clientHeight > maxSize) {
                maxSize = document.documentElement.clientHeight;
            }
        }

        return maxSize;
    };

    // HTML5 video detection function
    this.video = function () {
        var support = false;

        try {
            support = !!document.createElement("video").canPlayType;
        } catch (ignore) { }

        return support;
    };

    // HTML5 video mimetype detection function
    this.testVideo = function (mimetype) {
        var support = false;

        try {
            support = !!document.createElement("video").canPlayType(mimetype).replace(/^no$/, "");
        } catch (ignore) { }

        return support;
    };

    // MP4/H264 detection
    this.h264 = this.testVideo('video/mp4; codecs="avc1.42E01E"');

    // MP4/H.265 detection
    this.h265 = this.testVideo('video/mp4; codecs="hvc1.1.L0.0"');

    // MP4/AV1 detection
    this.av1 = this.testVideo('video/mp4; codecs="av01.0.05M.08"');

    // Ogg Theora detection
    this.ogg = this.testVideo('video/ogg; codecs="theora"');

    // WebM detection
    this.webm = this.testVideo('video/webm; codecs="vp8"');

    // WebM VP9 detection
    this.vp9Webm = this.testVideo('video/webm; codecs="vp9"');

    // WebM AV1 detection
    this.av1Webm = this.testVideo('video/webm; codecs="av01.0.05M.08"');

    // HLS
    this.hls = this.testVideo('application/x-mpegurl; codecs="avc1.42E01E"');

    // MPEG-DASH
    this.dash = window.hasOwnProperty("MediaSource");

    // HTML5 video autoplay detection function
    this.testAutoplay = function (callback, wait) {
        if (!this.ogg && !this.h264) {
            callback(false);
        }

        var el = document.createElement("video");

        el.setAttribute("autoplay", "autoplay");
        el.style.position = "absolute";
        el.style.height = "0";
        el.style.width = "0";

        if (this.ogg) {
            el.src = 'data:video/ogg;base64,T2dnUwACAAAAAAAAAABmnCATAAAAAHDEixYBKoB0aGVvcmEDAgEAAQABAAAQAAAQAAAAAAAFAAAAAQAAAAAAAAAAAGIAYE9nZ1MAAAAAAAAAAAAAZpwgEwEAAAACrA7TDlj///////////////+QgXRoZW9yYSsAAABYaXBoLk9yZyBsaWJ0aGVvcmEgMS4xIDIwMDkwODIyIChUaHVzbmVsZGEpAQAAABoAAABFTkNPREVSPWZmbXBlZzJ0aGVvcmEtMC4yOYJ0aGVvcmG+zSj3uc1rGLWpSUoQc5zmMYxSlKQhCDGMYhCEIQhAAAAAAAAAAAAAEW2uU2eSyPxWEvx4OVts5ir1aKtUKBMpJFoQ/nk5m41mUwl4slUpk4kkghkIfDwdjgajQYC8VioUCQRiIQh8PBwMhgLBQIg4FRba5TZ5LI/FYS/Hg5W2zmKvVoq1QoEykkWhD+eTmbjWZTCXiyVSmTiSSCGQh8PB2OBqNBgLxWKhQJBGIhCHw8HAyGAsFAiDgUCw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDw8PDAwPEhQUFQ0NDhESFRUUDg4PEhQVFRUOEBETFBUVFRARFBUVFRUVEhMUFRUVFRUUFRUVFRUVFRUVFRUVFRUVEAwLEBQZGxwNDQ4SFRwcGw4NEBQZHBwcDhATFhsdHRwRExkcHB4eHRQYGxwdHh4dGxwdHR4eHh4dHR0dHh4eHRALChAYKDM9DAwOExo6PDcODRAYKDlFOA4RFh0zV1A+EhYlOkRtZ00YIzdAUWhxXDFATldneXhlSFxfYnBkZ2MTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTExMTEhIVGRoaGhoSFBYaGhoaGhUWGRoaGhoaGRoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhoaGhESFh8kJCQkEhQYIiQkJCQWGCEkJCQkJB8iJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQkJCQREhgvY2NjYxIVGkJjY2NjGBo4Y2NjY2MvQmNjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRISEhUXGBkbEhIVFxgZGxwSFRcYGRscHRUXGBkbHB0dFxgZGxwdHR0YGRscHR0dHhkbHB0dHR4eGxwdHR0eHh4REREUFxocIBERFBcaHCAiERQXGhwgIiUUFxocICIlJRcaHCAiJSUlGhwgIiUlJSkcICIlJSUpKiAiJSUlKSoqEBAQFBgcICgQEBQYHCAoMBAUGBwgKDBAFBgcICgwQEAYHCAoMEBAQBwgKDBAQEBgICgwQEBAYIAoMEBAQGCAgAfF5cdH1e3Ow/L66wGmYnfIUbwdUTe3LMRbqON8B+5RJEvcGxkvrVUjTMrsXYhAnIwe0dTJfOYbWrDYyqUrz7dw/JO4hpmV2LsQQvkUeGq1BsZLx+cu5iV0e0eScJ91VIQYrmqfdVSK7GgjOU0oPaPOu5IcDK1mNvnD+K8LwS87f8Jx2mHtHnUkTGAurWZlNQa74ZLSFH9oF6FPGxzLsjQO5Qe0edcpttd7BXBSqMCL4k/4tFrHIPuEQ7m1/uIWkbDMWVoDdOSuRQ9286kvVUlQjzOE6VrNguN4oRXYGkgcnih7t13/9kxvLYKQezwLTrO44sVmMPgMqORo1E0sm1/9SludkcWHwfJwTSybR4LeAz6ugWVgRaY8mV/9SluQmtHrzsBtRF/wPY+X0JuYTs+ltgrXAmlk10xQHmTu9VSIAk1+vcvU4ml2oNzrNhEtQ3CysNP8UeR35wqpKUBdGdZMSjX4WVi8nJpdpHnbhzEIdx7mwf6W1FKAiucMXrWUWVjyRf23chNtR9mIzDoT/6ZLYailAjhFlZuvPtSeZ+2oREubDoWmT3TguY+JHPdRVSLKxfKH3vgNqJ/9emeEYikGXDFNzaLjvTeGAL61mogOoeG3y6oU4rW55ydoj0lUTSR/mmRhPmF86uwIfzp3FtiufQCmppaHDlGE0r2iTzXIw3zBq5hvaTldjG4CPb9wdxAme0SyedVKczJ9AtYbgPOzYKJvZZImsN7ecrxWZg5dR6ZLj/j4qpWsIA+vYwE+Tca9ounMIsrXMB4Stiib2SPQtZv+FVIpfEbzv8ncZoLBXc3YBqTG1HsskTTotZOYTG+oVUjLk6zhP8bg4RhMUNtfZdO7FdpBuXzhJ5Fh8IKlJG7wtD9ik8rWOJxy6iQ3NwzBpQ219mlyv+FLicYs2iJGSE0u2txzed++D61ZWCiHD/cZdQVCqkO2gJpdpNaObhnDfAPrT89RxdWFZ5hO3MseBSIlANppdZNIV/Rwe5eLTDvkfWKzFnH+QJ7m9QWV1KdwnuIwTNtZdJMoXBf74OhRnh2t+OTGL+AVUnIkyYY+QG7g9itHXyF3OIygG2s2kud679ZWKqSFa9n3IHD6MeLv1lZ0XyduRhiDRtrNnKoyiFVLcBm0ba5Yy3fQkDh4XsFE34isVpOzpa9nR8iCpS4HoxG2rJpnRhf3YboVa1PcRouh5LIJv/uQcPNd095ickTaiGBnWLKVWRc0OnYTSyex/n2FofEPnDG8y3PztHrzOLK1xo6RAml2k9owKajOC0Wr4D5x+3nA0UEhK2m198wuBHF3zlWWVKWLN1CHzLClUfuoYBcx4b1llpeBKmbayaR58njtE9onD66lUcsg0Spm2snsb+8HaJRn4dYcLbCuBuYwziB8/5U1C1DOOz2gZjSZtrLJk6vrLF3hwY4Io9xuT/ruUFRSBkNtUzTOWhjh26irLEPx4jPZL3Fo3QrReoGTTM21xYTT9oFdhTUIvjqTkfkvt0bzgVUjq/hOYY8j60IaO/0AzRBtqkTS6R5ellZd5uKdzzhb8BFlDdAcrwkE0rbXTOPB+7Y0FlZO96qFL4Ykg21StJs8qIW7h16H5hGiv8V2Cflau7QVDepTAHa6Lgt6feiEvJDM21StJsmOH/hynURrKxvUpQ8BH0JF7BiyG2qZpnL/7AOU66gt+reLEXY8pVOCQvSsBtqZTNM8bk9ohRcwD18o/WVkbvrceVKRb9I59IEKysjBeTMmmbA21xu/6iHadLRxuIzkLpi8wZYmmbbWi32RVAUjruxWlJ//iFxE38FI9hNKOoCdhwf5fDe4xZ81lgREhK2m1j78vW1CqkuMu/AjBNK210kzRUX/B+69cMMUG5bYrIeZxVSEZISmkzbXOi9yxwIfPgdsov7R71xuJ7rFcACjG/9PzApqFq7wEgzNJm2suWESPuwrQvejj7cbnQxMkxpm21lUYJL0fKmogPPqywn7e3FvB/FCNxPJ85iVUkCE9/tLKx31G4CgNtWTTPFhMvlu8G4/TrgaZttTChljfNJGgOT2X6EqpETy2tYd9cCBI4lIXJ1/3uVUllZEJz4baqGF64yxaZ+zPLYwde8Uqn1oKANtUrSaTOPHkhvuQP3bBlEJ/LFe4pqQOHUI8T8q7AXx3fLVBgSCVpMba55YxN3rv8U1Dv51bAPSOLlZWebkL8vSMGI21lJmmeVxPRwFlZF1CpqCN8uLwymaZyjbXHCRytogPN3o/n74CNykfT+qqRv5AQlHcRxYrC5KvGmbbUwmZY/29BvF6C1/93x4WVglXDLFpmbapmF89HKTogRwqqSlGbu+oiAkcWFbklC6Zhf+NtTLFpn8oWz+HsNRVSgIxZWON+yVyJlE5tq/+GWLTMutYX9ekTySEQPLVNQQ3OfycwJBM0zNtZcse7CvcKI0V/zh16Dr9OSA21MpmmcrHC+6pTAPHPwoit3LHHqs7jhFNRD6W8+EBGoSEoaZttTCZljfduH/fFisn+dRBGAZYtMzbVMwvul/T/crK1NQh8gN0SRRa9cOux6clC0/mDLFpmbarmF8/e6CopeOLCNW6S/IUUg3jJIYiAcDoMcGeRbOvuTPjXR/tyo79LK3kqqkbxkkMRAOB0GODPItnX3Jnxro/25Ud+llbyVVSN4ySGIgHA6DHBnkWzr7kz410f7cqO/Syt5KqpFVJwn6gBEvBM0zNtZcpGOEPiysW8vvRd2R0f7gtjhqUvXL+gWVwHm4XJDBiMpmmZtrLfPwd/IugP5+fKVSysH1EXreFAcEhelGmbbUmZY4Xdo1vQWVnK19P4RuEnbf0gQnR+lDCZlivNM22t1ESmopPIgfT0duOfQrsjgG4tPxli0zJmF5trdL1JDUIUT1ZXSqQDeR4B8mX3TrRro/2McGeUvLtwo6jIEKMkCUXWsLyZROd9P/rFYNtXPBli0z398iVUlVKAjFlY437JXImUTm2r/4ZYtMy61hf16RPJIU9nZ1MABAwAAAAAAAAAZpwgEwIAAABhp658BScAAAAAAADnUFBQXIDGXLhwtttNHDhw5OcpQRMETBEwRPduylKVB0HRdF0A';
        } else if (this.h264) {
            el.src = 'data:video/mp4;base64,AAAAHGZ0eXBtcDQyAAAAAG1wNDJpc29tYXZjMQAAAz5tb292AAAAbG12aGQAAAAAzaNacc2jWnEAAV+QAAFfkAABAAABAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACAAAAGGlvZHMAAAAAEICAgAcAT////3//AAACQ3RyYWsAAABcdGtoZAAAAAHNo1pxzaNacQAAAAEAAAAAAAFfkAAAAAAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAEAAAAAAEAAAABAAAAAAAd9tZGlhAAAAIG1kaGQAAAAAzaNacc2jWnEAAV+QAAFfkFXEAAAAAAAhaGRscgAAAAAAAAAAdmlkZQAAAAAAAAAAAAAAAAAAAAGWbWluZgAAABR2bWhkAAAAAQAAAAAAAAAAAAAAJGRpbmYAAAAcZHJlZgAAAAAAAAABAAAADHVybCAAAAABAAABVnN0YmwAAACpc3RzZAAAAAAAAAABAAAAmWF2YzEAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAEAAQAEgAAABIAAAAAAAAAAEOSlZUL0FWQyBDb2RpbmcAAAAAAAAAAAAAAAAAAAAAAAAY//8AAAAxYXZjQwH0AAr/4QAZZ/QACq609NQYBBkAAAMAAQAAAwAKjxImoAEABWjOAa8gAAAAEmNvbHJuY2xjAAYAAQAGAAAAGHN0dHMAAAAAAAAAAQAAAAUAAEZQAAAAKHN0c3oAAAAAAAAAAAAAAAUAAAIqAAAACAAAAAgAAAAIAAAACAAAAChzdHNjAAAAAAAAAAIAAAABAAAABAAAAAEAAAACAAAAAQAAAAEAAAAYc3RjbwAAAAAAAAACAAADYgAABaQAAAAUc3RzcwAAAAAAAAABAAAAAQAAABFzZHRwAAAAAAREREREAAAAb3VkdGEAAABnbWV0YQAAAAAAAAAhaGRscgAAAAAAAAAAbWRpcgAAAAAAAAAAAAAAAAAAAAA6aWxzdAAAADKpdG9vAAAAKmRhdGEAAAABAAAAAEhhbmRCcmFrZSAwLjkuOCAyMDEyMDcxODAwAAACUm1kYXQAAAHkBgX/4NxF6b3m2Ui3lizYINkj7u94MjY0IC0gY29yZSAxMjAgLSBILjI2NC9NUEVHLTQgQVZDIGNvZGVjIC0gQ29weWxlZnQgMjAwMy0yMDExIC0gaHR0cDovL3d3dy52aWRlb2xhbi5vcmcveDI2NC5odG1sIC0gb3B0aW9uczogY2FiYWM9MCByZWY9MSBkZWJsb2NrPTE6MDowIGFuYWx5c2U9MHgxOjAgbWU9ZXNhIHN1Ym1lPTkgcHN5PTAgbWl4ZWRfcmVmPTAgbWVfcmFuZ2U9NCBjaHJvbWFfbWU9MSB0cmVsbGlzPTAgOHg4ZGN0PTAgY3FtPTAgZGVhZHpvbmU9MjEsMTEgZmFzdF9wc2tpcD0wIGNocm9tYV9xcF9vZmZzZXQ9MCB0aHJlYWRzPTYgc2xpY2VkX3RocmVhZHM9MCBucj0wIGRlY2ltYXRlPTEgaW50ZXJsYWNlZD0wIGJsdXJheV9jb21wYXQ9MCBjb25zdHJhaW5lZF9pbnRyYT0wIGJmcmFtZXM9MCB3ZWlnaHRwPTAga2V5aW50PTUwIGtleWludF9taW49NSBzY2VuZWN1dD00MCBpbnRyYV9yZWZyZXNoPTAgcmM9Y3FwIG1idHJlZT0wIHFwPTAAgAAAAD5liISscR8A+E4ACAACFoAAITAAAgsAAPgYCoKgoC+L4vi+KAvi+L4YfAEAACMzgABF9AAEUGUgABDJiXnf4AAAAARBmiKUAAAABEGaQpQAAAAEQZpilAAAAARBmoKU';
        }

        document.documentElement.appendChild(el);

        if (!wait) {
            wait = 300;
        }

        window.setTimeout(function () {
            var autoplay = false;
            if (!el.paused || el.currentTime > 0) {
                autoplay = true;
            }
            el.parentNode.removeChild(el);

            callback(autoplay);
        }, wait);
    };

    // Plugin detection
    this.testPlugin = function (object, mimetype) {
        var support = false;

        try {
            var o = new ActiveXObject(object);
            if (o) {
                support = true;
            }
        } catch (e) {
            var m = navigator.mimeTypes[mimetype];
            if (m && m.enabledPlugin) {
                support = true;
            }
        }

        return support;
    };

    // Flash detection
    this.flash = this.testPlugin("ShockwaveFlash.ShockwaveFlash", "application/x-shockwave-flash");

    // Embed Flash player
    this.flashPlayer = function (options) {
        if (typeof options !== "object" || !options.id || !options.flashvars || !options.player) {
            console.log("videohelper.flashPlayer: invalid options");
            return false;
        }

        var element = document.getElementById(options.id);
        if (!element) {
            console.log("videohelper.flashPlayer ID " + options.id + " not found");
            return false;
        }

        if (isNaN(options.width) && isNaN(options.height)) {
            options.width = this.options.width;
            options.height = this.options.height;
        }

        element.innerHTML = '<object type="application/x-shockwave-flash" data="' + options.player + '" width="' + options.width + '" height="' + options.height + '>' +
        '<param name="movie" value="' + options.player + '" />' +
        '<param name="allowFullScreen" value="true" />' +
        '<param name="flashvars" value="' + options.flashvars + '" />' +
        '</object>';
    };

    // Windows Media detection
    this.wmvWmp = this.testPlugin("WMPlayer.OCX", "video/x-ms-wmv");

    // MP4 Windows Media detection
    this.mp4Wmp = this.testPlugin("WMPlayer.OCX", "video/mp4");

    // Embed Windows Media player
    this.wmPlayer = function (options) {
        if (!options.id || !options.file) {
            console.log("videohelper.wmPlayer: invalid options");
            return false;
        }

        var element = document.getElementById(options.id);
        if (!element) {
            console.log("videohelper.wmPlayer ID " + options.id + " not found");
            return false;
        }

        if (isNaN(options.width) && isNaN(options.height)) {
            options.width = this.options.width;
            options.height = this.options.height;
        }

        options.showcontrols = (typeof options.showcontrols === 'boolean' && !options.showcontrols) ? false : true;

        if (options.showcontrols) {
            options.height += 46;
        }

        options.autoplay = (typeof options.autoplay === 'boolean' && options.showcontrols) ? true : false;

        element.innerHTML =
        '<object type="application/x-oleobject" data="' + options.file + '" width="' + options.width + '" height="' + options.height + '">' +
        '<param name="url" value="' + options.file + '">' +
        '<param name="filename" value="' + options.file + '">' +
        '<param name="showcontrols" value="' + options.showcontrols + '">' +
        '<param name="autoplay" value="' + options.autoplay + '">' +
        '<embed type="application/x-mplayer2" src="' + options.file + '" autostart="' + options.autoplay + '" showcontrols="' + options.showcontrols + '" width="' + options.width + ' height="' + options.height + '" pluginspage="http://www.microsoft.com/Windows/MediaPlayer/"></embed>' +
        '</object>';
    };

    // QuickTime detection
    this.quicktime = this.testPlugin("QuickTime.QuickTime", "video/quicktime");

    // QuickTime MP4 detection
    this.mp4Quicktime = this.testPlugin("QuickTime.QuickTime", "video/mp4");

    // Embed QuickTime player
    this.quicktimePlayer = function (options) {
        if (!options.id || !options.file) {
            console.log("videohelper.quicktimePlayer: invalid options");
            return false;
        }

        var element = document.getElementById(options.id);
        if (!element) {
            console.log("videohelper.quicktimePlayer ID " + options.id + " not found");
            return false;
        }

        if (isNaN(options.width) && isNaN(options.height)) {
            options.width = this.options.width;
            options.height = this.options.height;
        }

        options.showcontrols = (typeof options.showcontrols === 'boolean' && !options.showcontrols) ? false : true;

        options.autoplay = (typeof options.autoplay === 'boolean' && options.showcontrols) ? true : false;

        element.innerHTML =
        '<object type="video/quicktime" data="' + options.file + '" width="' + options.width + ' height="' + options.height + '" codebase="http://www.apple.com/qtactivex/qtplugin.cab">' +
        '<param name="scale" value="aspect">' +
        '<param name="src" value="' + options.file + '">' +
        '<param name="autoplay" value="' + options.autoplay + '">' +
        '<param name="controller" value="' + options.showcontrols + '">' +
        '<embed type="video/quicktime" src="' + options.file + '" width="' + options.width + ' height="' + options.height + '" autoplay="' + options.autoplay + '" controller="' + options.showcontrols + '" pluginspage="http://www.apple.com/quicktime/download/"></embed>' +
        '</object>';
    };
}
